<?php

/*
Plugin Name: foursquarefrontpage
Description: test
Version: 0.0.1
*/

class FourSquareFrontpage
{
	public static $url;

	public function __construct() 
	{
		self::$url = plugins_url("blueoceantest");

		require_once("pages.php");
		require_once("customtypepost.php");
	}
}

new FourSquareFrontpage();

