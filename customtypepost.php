<?php

class CustomTypePost 
{
	public function __construct() {
		add_action("init",[$this,"set_cpt_newpage"]);
		add_action("add_meta_boxes",[$this,"set_meta_boxes"]);
		add_action("save_post",[$this,"save_custom_post_type"]);	
	}

	public function set_cpt_newpage() {
		register_post_type("newpage",[
				"label" => "Newpage",
				"public" => true,
				"hierarchical" => true,
				"supports" => ["title","editor","thumbnail"]
			]);
	}

	public function set_meta_boxes() {
		add_meta_box(
			"wpslider_upload",
			"newpage setup",
			[$this,"newpage_data_dimensions_view"],
			"newpage",
			"side",
			"default"
			);
	}

	public function newpage_data_dimensions_view($data) {
		$articletype = get_post_meta($data->ID, "articletype",true);
		$readmore = get_post_meta($data->ID, "readmore",true);
		$linktype = get_post_meta($data->ID, "linktype",true);
		$linktypes[$linktype] = "selected";
		$link = get_post_meta($data->ID, "link",true);
		$position = get_post_meta($data->ID, "position",true);
		$positions[$position] = "checked";
		require_once("views/newpage-data-dimensions.php");
	}

	public function save_custom_post_type($post_id) {
		if (isset($_POST["articletype"]) && !empty($_POST["articletype"]) ) {
			update_post_meta($post_id,"articletype",$_POST["articletype"]);
		}
		if (isset($_POST["readmore"]) && !empty($_POST["readmore"]) ) {
			update_post_meta($post_id,"readmore",$_POST["readmore"]);
		}
		if (isset($_POST["linktype"]) && !empty($_POST["linktype"]) ) {
			update_post_meta($post_id,"linktype",$_POST["linktype"]);
		}
		if (isset($_POST["link"]) && !empty($_POST["link"]) ) {
			update_post_meta($post_id,"link",$_POST["link"]);
		}
		if (isset($_POST["position"]) && !empty($_POST["position"]) ) {
			update_post_meta($post_id,"position",$_POST["position"]);
		}

	} 
}

new CustomTypePost();
?>