<?php


class Pages {

	public function __construct() {
		add_action("admin_enqueue_scripts",[$this,"styles"]);
		add_action("wp_enqueue_scripts",[$this,"styles"]);
	}

	public function styles() {
		$this->set_styles();
	}

	private function set_styles() {
		wp_enqueue_style(	"test-main",
							FourSquareFrontpage::$url . "/assets/css/main.css",
							[],
							null,
							"screen");
	}

}

new Pages();





