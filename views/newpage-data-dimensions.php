<div class="wrap">
	<div class="form-control">
		<label>
			article type:<br>
			<input type="text" name="articletype" class="large-text" value="<?=$articletype?>">
		</label>
	</div>
	<div class="form-control">
		<label>
			read more:<br>
			<input type="text" name="readmore" class="large-text" value="<?=$readmore?>">
		</label>
	</div>
	<div class="form-control">
		<label>
			link type<br>
			<select name="linktype">
				<option <?php echo $linktypes["page link"] ?> >page link
				<option <?php echo $linktypes["modal"] ?> >modal
			</select>
		</label>
	</div>
	<div class="form-control">
		<label>
			link<br>
			<input type="text" name="link" class="large-text" value="<?=$link?>">
		</label>
	</div>
	<div class="form-control">
		<label>
			position on frontpage<br>
			<table>
				<tr>
					<td class="td1">
						1
						<input type="radio" name="position" value="1" <?php echo $positions[1] ?> >
					</td>
					<td class="td1">
						2
						<input type="radio" name="position" value="2" <?php echo $positions[2] ?> >
					</td>
				</tr>
				<tr>
					<td class="td1">
						3
						<input type="radio" name="position" value="3" <?php echo $positions[3] ?> >
					</td>
					<td class="td1">
						4
						<input type="radio" name="position" value="4" <?php echo $positions[4] ?> >
					</td>
				</tr>
			</table>
		</label>
	</div>
</div>

